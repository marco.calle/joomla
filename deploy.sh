#!/bin/bash

##Deploy de DB para Joomla
microk8s.kubectl apply -f joomladb-pv.yaml && \
microk8s.kubectl apply -f joomladb-pvc.yaml && \
microk8s.kubectl apply -f joomladb-deployment.yaml && echo 'Deploy de DB finalizado'


##Deploy de Joomla
microk8s.kubectl apply -f joomla-tp-pv.yaml && \
microk8s.kubectl apply -f joomla-tp-pvc.yaml && \
microk8s.kubectl apply -f joomla-tp-deployment.yaml && echo 'Deploy Joomla finalizado'

##Exponer IP del cluster por puerto aleatorio
microk8s.kubectl expose deployment joomladb --type=NodePort --port=3306 --name=joomladb
microk8s.kubectl expose deployment joomla-tp --type=NodePort --port=80 --name=joomla-tp

echo 'Acceder al sitio joomla por puerto: '$(microk8s.kubectl get service | grep joomla-tp | cut -d ':' -f 2 | cut -d '/' -f 1)
echo 'Para DB ingresar: ip_host:'$(microk8s.kubectl get service | grep joomladb | cut -d ':' -f 2 | cut -d '/' -f 1)
