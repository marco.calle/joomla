#!/bin/bash
set -e

#Se verifica si ya se realizó la instalación de joomla
#en caso negativo se descomprime todo el paquete de instalación

if [ ! -f /var/www/html/configurado.ctl ]; then
        echo 'ServerName joomla.com' >> /etc/apache2/apache2.conf
        cp /app/Joomla_3-9-28-Stable-Full_Package.zip /var/www/html
        cd /var/www/html && unzip -o Joomla_3-9-28-Stable-Full_Package.zip
        chown -R www-data:www-data /var/www/html
        rm /var/www/html/Joomla_3-9-28-Stable-Full_Package.zip
        touch /var/www/html/configurado.ctl
fi

apachectl -D FOREGROUND

exec "$@"

