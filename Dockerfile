FROM php:7.4-apache
LABEL maintainer="marco.calle@istea.com.ar"
LABEL version="1.0"
LABEL description="TP Final - Actualización Tecnologica - joomla"

#Se expone el puerto
EXPOSE 80

#Argumento que indica que no se interactuará con el prompt en el momento de la instalacion de componentes
ARG DEBIAN_FRONTEND=noninteractive

#Deshabilita la creacion de archivo txt para verificar la instalacion de Joomla con una base de datos externa
ENV JOOMLA_INSTALLATION_DISABLE_LOCALHOST_CHECK 1 

RUN apt update && apt install -y unzip
RUN docker-php-ext-install mysqli && docker-php-ext-enable mysqli

RUN mkdir -p /app

#Copiando paquete
COPY Joomla_3-9-28-Stable-Full_Package.zip /app

#Se define volumen html
VOLUME ["/var/www/html"]

#Entrypoint para inicializar Apache e instalar Joomla
COPY entrypoint.sh /
RUN chmod +x /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
