# Joomla 
## Actualización tecnológica - TP - FINAL

### Componentes:
- Servidor web: php:7.4-apache
- Servidor Mysql: mysql:5.7
- Aplicación: Joomla 3.9.28 https://downloads.joomla.org/cms/joomla3/3-9-28/Joomla_3-9-28-Stable-Full_Package.zip

### Se crea la imagen mcalle86/joomla-tp:1.1 tomando como base php:7.4-apache
- Se instala y habilita la extensión mysqli que no es nativa de la imagen.
- Se agrega el instalador de Joomla

### Se crea la imagen mcalle86/joomladb:1.0 tomando como base mysql:5.7
Se agrega variables de entorno con los siguientes datos: \
ENV MYSQL_USER joomla \
ENV MYSQL_PASSWORD q1w2e3r4 \
ENV MYSQL_DATABASE joomla \
ENV MYSQL_ROOT_PASSWORD q1w2e3r4

##### La instalación web no reconoce la DB gnerada por deploy por lo cual se crea un servicio tipo NodePort para poder acceder a la DB y completar la instalación

### Despliegue
- Se desarrollo el script deploy.sh que automatiza la creación de los volumenes persistentes, los reclamos de espacio y la creación de los PODS y servicios. Al finalizar informa los puertos para poder acceder a la aplicación web y a la DB.
- Se agrega el script borra-deply.sh para eliminar todos los componenes del proyecto.

