#!/bin/bash
set -e

# Iniciar mysql server para poder ejecutar los comandos posteriores
/etc/init.d/mysql start 
 
# Esperamos 5 segundos para que cargue mysql
sleep 5
 
if [ ! -f /app/mysql.configured ]; then
        mysql -u root -e "CREATE USER '$MYSQL_USER'@'localhost' IDENTIFIED BY '$MYSQL_USER_PASSWORD';"
        touch /app/mysql.configured
fi

# Crear base de datos

if [ ! -f /app/mysql.database.configured ]; then
    if [ $MYSQL_DB_NAME == "wordpress" ];then

        mysql -u root -e "CREATE DATABASE $MYSQL_DB_NAME;"
        mysql -u root -e "GRANT ALL PRIVILEGES ON *.* TO '$MYSQL_USER'@'localhost';"
        touch /app/mysql.database.configured
    else
        mysql -u root -e "CREATE DATABASE $MYSQL_DB_NAME;"
        mysql -u root -e "GRANT ALL PRIVILEGES ON *.* TO '$MYSQL_USER'@'localhost';"
        touch /app/mysql.database.configured
    fi
fi

exec "$@"
