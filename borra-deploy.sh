#!/bin/bash

microk8s.kubectl delete service joomladb && \
microk8s.kubectl delete service joomla-tp && \
microk8s.kubectl delete deployment --all && \
microk8s.kubectl delete pods --all && \
microk8s.kubectl delete pvc --all && \
microk8s.kubectl delete pv --all && \


rm -r /mnt/* && echo 'Directorio de volumenes persistentes borrados' 
